#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct Location
{
	string name, description;
	int index;
	int toNorth, toSouth, toEast, toWest;

	Location( const string& name, const string& description, int index )
	{
		Setup( name, description, index );
	}

	void Setup( const string& name, const string& description, int index )
	{
		this->name = name;
		this->description = description;
		this->index = index;
		this->toNorth = this->toSouth = this->toEast = this->toWest = -1;
	}
};

void DisplayRoom( vector<Location>& world, int currentRoom );
int SetupWorld( vector<Location>& world );
string GetCommand( vector<string> commands );
void HandleCommand( const string& command, vector<Location>& world, int& currentRoom );

int main()
{
	bool done = false;
	vector<Location> world;
	int currentRoom = SetupWorld( world );

	vector<string> commands;
	commands.push_back( "Hun - Im hunedi" );	// Travel north
	commands.push_back( "Han - Im hanedi" );	// Travel south
	commands.push_back( "Hene - Im henedi" );	// Travel east
	commands.push_back( "Hon - Im honedi" );	// Travel west

	string command = "";

	cout << "IM BETHEDI" << endl;

	while ( !done )
	{
		DisplayRoom( world, currentRoom );
		HandleCommand( GetCommand( commands ), world, currentRoom );
	}

	return 0;
}

void HandleCommand( const string& command, vector<Location>& world, int& currentRoom )
{
	if ( command == "Hun" || command == "hun" &&		// North
		world[ currentRoom ].toNorth != -1 )
	{
		cout << "\n Bíide sháad le hunedi wo." << endl;
		currentRoom = world[ currentRoom ].toNorth;
	}

	else if ( command == "Han" || command == "han" &&	// South
		world[ currentRoom ].toSouth != -1 )
	{
		cout << "\n Bíide sháad le hanedi wo." << endl;
		currentRoom = world[ currentRoom ].toSouth;
	}


	else if ( command == "Hene" || command == "hene" &&	// East
		world[ currentRoom ].toEast != -1 )
	{
		cout << "\n Bíide sháad le henedi wo." << endl;
		currentRoom = world[ currentRoom ].toEast;
	}


	else if ( command == "Hon" || command == "hon" &&	// West
		world[ currentRoom ].toWest != -1 )
	{
		cout << "\n Bíide sháad le honedi wo." << endl;
		currentRoom = world[ currentRoom ].toWest;
	}

	else
	{
		cout << "\n Bíide thad ra sháad wethenedi wo." << endl; // Can't go that way.
	}
}

string GetCommand( vector<string> commands )
{
	cout << "Báa néde shub le bebáa?" << endl; // "What do you want to do?"

	cout << endl;
	cout << "\t\t" << commands[0] << endl;
	cout << commands[3] << "\t\t\t" << commands[2] << endl;
	cout << "\t\t" << commands[1] << endl;

	cout << endl << ">> ";

	string command;
	cin >> command;
	return command;
}

void DisplayRoom( vector<Location>& world, int currentRoom )
{
	cout << endl;
	cout << "Hoth: " << world[ currentRoom ].name << endl;
	cout << "Eth:  " << world[ currentRoom ].description << endl;
	cout << "Bíide thad sháad le: ";

	string canGo = "";
	if ( world[ currentRoom ].toNorth != -1 ) { canGo += "hunedi"; }
	if ( world[ currentRoom ].toSouth != -1 ) { if ( canGo != "" ) { canGo += ", "; } canGo += "hanedi"; }
	if ( world[ currentRoom ].toEast != -1 ) { if ( canGo != "" ) { canGo += ", "; } canGo += "henedi"; }
	if ( world[ currentRoom ].toWest != -1 ) { if ( canGo != "" ) { canGo += ", "; } canGo += "honedi"; }

	cout << canGo << " wo." << endl;
	cout << endl;
}

int SetupWorld( vector<Location>& world )
{
	world.push_back( Location( "Beth", "Bíide hothal beth wo.", world.size() ) ); // home	0
	world.push_back( Location( "Olin", "Bi'ide rahíya olin wo.", world.size() ) ); // forest	1
	world.push_back( Location( "Wethud", "Bíide balin wethud wo.", world.size() ) ); // stone road	2
	world.push_back( Location( "Bethud", "Bíide rahith bethud wo.", world.size() ) ); // cave	3
	world.push_back( Location( "Miwith", "Bíide thi miwith méwitheth wo.", world.size() ) ); // town	4
	world.push_back( Location( "Áabewehe", "Bíide áya áabewehe wo.", world.size() ) ); // bookstore	5

	world[5].toNorth = 4;

	world[4].toSouth = 5;
	world[4].toWest = 3;
	world[4].toEast = 2;

	world[3].toEast = 4;

	world[2].toWest = 4;
	world[2].toNorth = 1;

	world[1].toSouth = 2;
	world[1].toNorth = 0;

	world[0].toSouth = 1;

	return 5; // starting point
}
